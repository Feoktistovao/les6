
package ru.feoktistov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.feoktistov.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClearTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "clearTask");
    private final static QName _ClearTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "clearTaskResponse");
    private final static QName _CreateTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "createTask");
    private final static QName _CreateTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "createTaskResponse");
    private final static QName _FindAllTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findAllTask");
    private final static QName _FindAllTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findAllTaskResponse");
    private final static QName _FindByIdTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findByIdTask");
    private final static QName _FindByIdTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findByIdTaskResponse");
    private final static QName _FindByIndexTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findByIndexTask");
    private final static QName _FindByIndexTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findByIndexTaskResponse");
    private final static QName _FindByNameTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findByNameTask");
    private final static QName _FindByNameTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "findByNameTaskResponse");
    private final static QName _RemoveByIdTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "removeByIdTask");
    private final static QName _RemoveByIdTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "removeByIdTaskResponse");
    private final static QName _RemoveByNameTask_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "removeByNameTask");
    private final static QName _RemoveByNameTaskResponse_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "removeByNameTaskResponse");
    private final static QName _Task_QNAME = new QName("http://endpoint.tm.feoktistov.ru/", "task");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.feoktistov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClearTask }
     * 
     */
    public ClearTask createClearTask() {
        return new ClearTask();
    }

    /**
     * Create an instance of {@link ClearTaskResponse }
     * 
     */
    public ClearTaskResponse createClearTaskResponse() {
        return new ClearTaskResponse();
    }

    /**
     * Create an instance of {@link CreateTask }
     * 
     */
    public CreateTask createCreateTask() {
        return new CreateTask();
    }

    /**
     * Create an instance of {@link CreateTaskResponse }
     * 
     */
    public CreateTaskResponse createCreateTaskResponse() {
        return new CreateTaskResponse();
    }

    /**
     * Create an instance of {@link FindAllTask }
     * 
     */
    public FindAllTask createFindAllTask() {
        return new FindAllTask();
    }

    /**
     * Create an instance of {@link FindAllTaskResponse }
     * 
     */
    public FindAllTaskResponse createFindAllTaskResponse() {
        return new FindAllTaskResponse();
    }

    /**
     * Create an instance of {@link FindByIdTask }
     * 
     */
    public FindByIdTask createFindByIdTask() {
        return new FindByIdTask();
    }

    /**
     * Create an instance of {@link FindByIdTaskResponse }
     * 
     */
    public FindByIdTaskResponse createFindByIdTaskResponse() {
        return new FindByIdTaskResponse();
    }

    /**
     * Create an instance of {@link FindByIndexTask }
     * 
     */
    public FindByIndexTask createFindByIndexTask() {
        return new FindByIndexTask();
    }

    /**
     * Create an instance of {@link FindByIndexTaskResponse }
     * 
     */
    public FindByIndexTaskResponse createFindByIndexTaskResponse() {
        return new FindByIndexTaskResponse();
    }

    /**
     * Create an instance of {@link FindByNameTask }
     * 
     */
    public FindByNameTask createFindByNameTask() {
        return new FindByNameTask();
    }

    /**
     * Create an instance of {@link FindByNameTaskResponse }
     * 
     */
    public FindByNameTaskResponse createFindByNameTaskResponse() {
        return new FindByNameTaskResponse();
    }

    /**
     * Create an instance of {@link RemoveByIdTask }
     * 
     */
    public RemoveByIdTask createRemoveByIdTask() {
        return new RemoveByIdTask();
    }

    /**
     * Create an instance of {@link RemoveByIdTaskResponse }
     * 
     */
    public RemoveByIdTaskResponse createRemoveByIdTaskResponse() {
        return new RemoveByIdTaskResponse();
    }

    /**
     * Create an instance of {@link RemoveByNameTask }
     * 
     */
    public RemoveByNameTask createRemoveByNameTask() {
        return new RemoveByNameTask();
    }

    /**
     * Create an instance of {@link RemoveByNameTaskResponse }
     * 
     */
    public RemoveByNameTaskResponse createRemoveByNameTaskResponse() {
        return new RemoveByNameTaskResponse();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "clearTask")
    public JAXBElement<ClearTask> createClearTask(ClearTask value) {
        return new JAXBElement<ClearTask>(_ClearTask_QNAME, ClearTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "clearTaskResponse")
    public JAXBElement<ClearTaskResponse> createClearTaskResponse(ClearTaskResponse value) {
        return new JAXBElement<ClearTaskResponse>(_ClearTaskResponse_QNAME, ClearTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "createTask")
    public JAXBElement<CreateTask> createCreateTask(CreateTask value) {
        return new JAXBElement<CreateTask>(_CreateTask_QNAME, CreateTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "createTaskResponse")
    public JAXBElement<CreateTaskResponse> createCreateTaskResponse(CreateTaskResponse value) {
        return new JAXBElement<CreateTaskResponse>(_CreateTaskResponse_QNAME, CreateTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findAllTask")
    public JAXBElement<FindAllTask> createFindAllTask(FindAllTask value) {
        return new JAXBElement<FindAllTask>(_FindAllTask_QNAME, FindAllTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findAllTaskResponse")
    public JAXBElement<FindAllTaskResponse> createFindAllTaskResponse(FindAllTaskResponse value) {
        return new JAXBElement<FindAllTaskResponse>(_FindAllTaskResponse_QNAME, FindAllTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIdTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByIdTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findByIdTask")
    public JAXBElement<FindByIdTask> createFindByIdTask(FindByIdTask value) {
        return new JAXBElement<FindByIdTask>(_FindByIdTask_QNAME, FindByIdTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIdTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByIdTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findByIdTaskResponse")
    public JAXBElement<FindByIdTaskResponse> createFindByIdTaskResponse(FindByIdTaskResponse value) {
        return new JAXBElement<FindByIdTaskResponse>(_FindByIdTaskResponse_QNAME, FindByIdTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIndexTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByIndexTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findByIndexTask")
    public JAXBElement<FindByIndexTask> createFindByIndexTask(FindByIndexTask value) {
        return new JAXBElement<FindByIndexTask>(_FindByIndexTask_QNAME, FindByIndexTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIndexTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByIndexTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findByIndexTaskResponse")
    public JAXBElement<FindByIndexTaskResponse> createFindByIndexTaskResponse(FindByIndexTaskResponse value) {
        return new JAXBElement<FindByIndexTaskResponse>(_FindByIndexTaskResponse_QNAME, FindByIndexTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByNameTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByNameTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findByNameTask")
    public JAXBElement<FindByNameTask> createFindByNameTask(FindByNameTask value) {
        return new JAXBElement<FindByNameTask>(_FindByNameTask_QNAME, FindByNameTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByNameTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByNameTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "findByNameTaskResponse")
    public JAXBElement<FindByNameTaskResponse> createFindByNameTaskResponse(FindByNameTaskResponse value) {
        return new JAXBElement<FindByNameTaskResponse>(_FindByNameTaskResponse_QNAME, FindByNameTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByIdTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveByIdTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "removeByIdTask")
    public JAXBElement<RemoveByIdTask> createRemoveByIdTask(RemoveByIdTask value) {
        return new JAXBElement<RemoveByIdTask>(_RemoveByIdTask_QNAME, RemoveByIdTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByIdTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveByIdTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "removeByIdTaskResponse")
    public JAXBElement<RemoveByIdTaskResponse> createRemoveByIdTaskResponse(RemoveByIdTaskResponse value) {
        return new JAXBElement<RemoveByIdTaskResponse>(_RemoveByIdTaskResponse_QNAME, RemoveByIdTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByNameTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveByNameTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "removeByNameTask")
    public JAXBElement<RemoveByNameTask> createRemoveByNameTask(RemoveByNameTask value) {
        return new JAXBElement<RemoveByNameTask>(_RemoveByNameTask_QNAME, RemoveByNameTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByNameTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveByNameTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "removeByNameTaskResponse")
    public JAXBElement<RemoveByNameTaskResponse> createRemoveByNameTaskResponse(RemoveByNameTaskResponse value) {
        return new JAXBElement<RemoveByNameTaskResponse>(_RemoveByNameTaskResponse_QNAME, RemoveByNameTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Task }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Task }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.feoktistov.ru/", name = "task")
    public JAXBElement<Task> createTask(Task value) {
        return new JAXBElement<Task>(_Task_QNAME, Task.class, null, value);
    }

}
