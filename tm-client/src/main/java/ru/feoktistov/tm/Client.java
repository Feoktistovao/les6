package ru.feoktistov.tm;

import ru.feoktistov.tm.endpoint.ProjectEndpoint;
import ru.feoktistov.tm.endpoint.ProjectEndpointService;


public class Client {

    public static void main(String[] args) {
        ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
        System.out.println(projectEndpoint.findAllProject());
    }

}
