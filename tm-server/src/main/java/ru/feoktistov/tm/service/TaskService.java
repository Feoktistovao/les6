package ru.feoktistov.tm.service;

import ru.feoktistov.tm.api.ConnectionProvider;
import ru.feoktistov.tm.entity.Task;
import ru.feoktistov.tm.repository.ProjectRepository;
import ru.feoktistov.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

public class TaskService {


    private final ConnectionProvider provider;

    public TaskService(ConnectionProvider provider) {
        this.provider = provider;
    }

    private TaskRepository getRepository(){
        return new TaskRepository(provider.getConnection());
    }


    public Task create(Task task) {
        if (task ==null) return null;
        return getRepository().create(task);
    }

    public void create(Collection<Task> tasks) {
        if (tasks==null || tasks.isEmpty())  return;
        getRepository().create(tasks);
    }

    public Task create(String name) {
        return getRepository().create(name);
    }

    public Task findByIndex(int index) {
        return getRepository().findByIndex(index);
    }

    public Task findByName(String name) {
        return getRepository().findByName(name);
    }

    public Task removeById(Long id) {
        return getRepository().removeById(id);
    }

    public Task removeByName(String name) {
        return getRepository().removeByName(name);
    }

    public Task findById(Long id) {
        return getRepository().findById(id);
    }

    public void clear() {
        getRepository().clear();
    }

    public List<Task> findAll() {
        return getRepository().findAll();
    }

}
