package ru.feoktistov.tm.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService {

    private static final String FILE = "/application.properties";

    private static final InputStream INPUT_STREAM = PropertyService.class.getResourceAsStream(FILE);
    private static final Properties properties = new Properties();;

    static {
        try {
            properties.load(INPUT_STREAM);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getJDBCPort() {
        return properties.getProperty("jdbc.port");
    }
    public String getJDBCHost() {
        return properties.getProperty("jdbc.host");
    }

    public String getJDBCDatabase(){
        return properties.getProperty("jdbc.database");
    }

    public String getJDBCUsername() {
        return properties.getProperty("jdbc.username");
    }

    public String getJDBCPassword() {
        return properties.getProperty("jdbc.password");
    }

    public String getJdbcURL(){
        return "jdbc:mysql://" +getJDBCHost() + ":" + getJDBCPort() + "/" + getJDBCDatabase()+"?serverTimezone=Europe/Moscow";
    }
}
