package ru.feoktistov.tm.service;

import ru.feoktistov.tm.api.ConnectionProvider;
import ru.feoktistov.tm.entity.Project;
import ru.feoktistov.tm.repository.ProjectRepository;


import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class ProjectService {



    private final ConnectionProvider provider;

    public ProjectService(ConnectionProvider provider) {
        this.provider = provider;
    }

    private ProjectRepository getRepository(){
        return new ProjectRepository(provider.getConnection());
    }

    public Project create(Project project) throws SQLException {
        if (project == null) return null;
        return getRepository().create(project);
    }

    public void create(Collection<Project> projects) throws SQLException {
        if (projects == null || projects.isEmpty()) return;
        getRepository().create(projects);

    }

    public Project create(String name) throws SQLException {
        if (name == null || name.isEmpty()) return null;
        return getRepository().create(name);
    }

    public Project create(String name, String description) throws SQLException {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;

        return getRepository().create(name, description);
    }

    public void update(Long id, String name, String description) throws SQLException {
        if (id == null) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
         getRepository().update(id, name, description);
    }

    public void clear() throws SQLException {
        getRepository().clear();
    }



    public Project findByName(String name) throws SQLException {
        return getRepository().findByName(name);
    }

    public Project findById(Long id) throws SQLException {
        return getRepository().findById(id);
    }



    public void removeById(Long id) throws SQLException {
         getRepository().removeById(id);
    }

    public void removeByName(String name) throws SQLException {
        getRepository().removeByName(name);
    }

    public List<Project> findAll() throws SQLException {
        return getRepository().findAll();
    }


    public void removeByIndex(int id) {
    }
}
