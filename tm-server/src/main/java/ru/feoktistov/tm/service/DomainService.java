package ru.feoktistov.tm.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ru.feoktistov.tm.entity.Domain;

import java.io.File;
import java.io.FileInputStream;

import java.nio.file.Files;
import java.sql.SQLException;


public class DomainService {

    private ProjectService projectService;
    private TaskService taskService;

    public DomainService(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public void load(Domain domain) throws SQLException {
        taskService.clear();
        projectService.clear();
        projectService.create(domain.getProjects());
        taskService.create(domain.getTasks());
    }

    public void save(Domain domain) throws SQLException {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setTasks(this.taskService.findAll());
    }

    public void exportXML() throws Exception {
        final Domain domain = new Domain();
        save(domain);
        final XmlMapper mapper = new XmlMapper() ;
        final String xml = mapper.writeValueAsString(domain);

        final byte[] data = xml.getBytes("UTF-8");
        final File file = new File("data.xml");
        Files.write(file.toPath(), data);
    }
    public void exportJSON() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        Domain domain = new Domain();
        save(domain);
        objectMapper.writeValue(new File("data.json"), domain);
    }
    public void exportBASE64() throws Exception{
        FileInputStream fileInputStream = new FileInputStream("data.txt");

        fileInputStream.close();


    }
    public void importXML() throws Exception{
        final File file = new File("data.xml");
        final byte[] bytes = Files.readAllBytes(file.toPath());
        final String xml = new String(bytes,"UTF-8");
        final XmlMapper xmlMapper = new XmlMapper();
        final Domain domain = xmlMapper.readValue(xml, Domain.class);
        load(domain);


    }
    public void importJSON() throws Exception{
        final File file = new File("data.json");
        final byte[] bytes = Files.readAllBytes(file.toPath());
        final String json = new String(bytes,"UTF-8");
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = objectMapper.readValue(json, Domain.class);
        load(domain);

    }
    public void importBASE64(){

    }
}
