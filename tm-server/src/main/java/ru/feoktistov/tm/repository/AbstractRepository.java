package ru.feoktistov.tm.repository;

import java.sql.Connection;

public class AbstractRepository {


    protected Connection connection;

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }
}
