package ru.feoktistov.tm.repository;

import ru.feoktistov.tm.entity.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

public class ProjectRepository extends AbstractRepository {

    public ProjectRepository(Connection connnection) {
        super(connnection);
    }



    public Project create(Project project) throws SQLException {
        String query = "insert into `project` (`id`,`name`,`description`) VALUES (?, ?, ?)";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setLong(1,project.getId());
        st.setString(2,project.getName());
        st.setString(3,project.getDescription());
        st.execute();
        return project;
    }

    public void create(Collection<Project> projects) throws SQLException {
        for (Project project : projects)
            create(project);
    }

    public Project create(final String name) throws SQLException {
       return create(new Project(name));
    }

    public Project create(final String name, final String description) throws SQLException {
        return create(new Project(name, description));
    }

    public void update(final Long id,final String name, final String description) throws SQLException {
        String query = "UPDATE `project` SET `name = ?,`description = ?` WHERE `id` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setString(1,name);
        st.setString(2,description);
        st.setLong(2,id);
        st.execute();
    }

    public void clear() throws SQLException {
        final String query = "DELETE FROM `project`";
        final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }



    public Project findByName(final String name) throws SQLException {
        String query = "select * from  `project` where `name` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setString(1,name);
        final ResultSet resultSet = st.executeQuery();
        Boolean hN = resultSet.next();
        if (!hN) return null;
        return fetch(resultSet);

    }

    private Project fetch(final ResultSet row) throws SQLException {
        if (row == null) return null;
        Project project = new Project();
        project.setId(row.getLong("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        return project;
    }


    public Project findById(final Long id) throws SQLException {
        if (id == null) return null;
        String query = "select * from  `project` where `id` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setLong(1,id);
        final ResultSet resultSet = st.executeQuery();
        Boolean hN = resultSet.next();
        if (!hN) return null;
        return fetch(resultSet);
    }



    public void removeById(final Long id) throws SQLException {
        if (id == null) return ;
        String query = "delete from  `project` where `id` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setLong(1,id);
        st.execute();
    }

    public void removeByName(final String name) throws SQLException {
        if (name== null) return ;
        String query = "delete from  `project` where `name` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setString(1,name);
        st.execute();
    }

    public List<Project> findAll() throws SQLException {
        String query = "select * from  `project` ";
        PreparedStatement st = getConnection().prepareStatement(query);
        final ResultSet resultSet = st.executeQuery();
        List<Project> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(fetch(resultSet));
        }
        st.close();
        return result;

    }


}
