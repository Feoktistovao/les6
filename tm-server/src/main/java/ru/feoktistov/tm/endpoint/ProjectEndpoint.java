package ru.feoktistov.tm.endpoint;

import ru.feoktistov.tm.entity.Project;
import ru.feoktistov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService
public class ProjectEndpoint {

    private ProjectService projectService;

    public ProjectEndpoint(ProjectService projectService) {
        this.projectService = projectService;
    }



    @WebMethod
    public Project createProject(
            @WebParam(name = "name") final String name) throws SQLException {
        return projectService.create(name);
    }

    @WebMethod
    public void clearProject() throws SQLException {
        projectService.clear();
    }

    @WebMethod
    public Project findByNameProject(
            @WebParam(name = "name")String name) throws SQLException {
        return projectService.findByName(name);
    }
    @WebMethod
    public Project findByIdProject(
            @WebParam(name = "id")Long id) throws SQLException {
        return projectService.findById(id);
    }

    @WebMethod
    public void removeByIdProject(
            @WebParam(name = "id")Long id) throws SQLException {
         projectService.removeById(id);
    }
    @WebMethod
    public void removeByNameProject(
            @WebParam(name = "name")String name) throws SQLException {
         projectService.removeByName(name);
    }
    @WebMethod
    public List<Project> findAllProject() throws SQLException {
        return projectService.findAll();
    }
}
