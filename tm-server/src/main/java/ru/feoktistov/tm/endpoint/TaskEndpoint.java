package ru.feoktistov.tm.endpoint;

import ru.feoktistov.tm.entity.Task;
import ru.feoktistov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint {

    private TaskService taskService;

    public TaskEndpoint() {

    }


    public TaskEndpoint(TaskService taskService) {
        this.taskService = taskService;
    }


    @WebMethod
    public Task createTask(
            @WebParam(name="name") String name) {
        return taskService.create(name);
    }
    @WebMethod
    public Task findByIndexTask(
            @WebParam(name="index") int index) {
        return taskService.findByIndex(index);
    }
    @WebMethod
    public Task findByNameTask(
            @WebParam(name="name") String name) {
        return taskService.findByName(name);
    }
    @WebMethod
    public Task removeByIdTask(
            @WebParam(name="id")Long id) {
        return taskService.removeById(id);
    }
    @WebMethod
    public Task removeByNameTask(
            @WebParam(name="name")String name) {
        return taskService.removeByName(name);
    }
    @WebMethod
    public Task findByIdTask(
            @WebParam(name="id")Long id) {
        return taskService.findById(id);
    }
    @WebMethod
    public void clearTask() {
        taskService.clear();
    }
    @WebMethod
    public List<Task> findAllTask() {
        return taskService.findAll();
    }
}
