package ru.feoktistov.tm.api;

import java.sql.Connection;

public interface ConnectionProvider {

    Connection getConnection();
}
