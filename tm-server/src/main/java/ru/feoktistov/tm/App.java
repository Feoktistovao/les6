package ru.feoktistov.tm;

import ru.feoktistov.tm.api.ConnectionProvider;
import ru.feoktistov.tm.controller.ProjectController;
import ru.feoktistov.tm.controller.SystemController;
import ru.feoktistov.tm.controller.TaskController;

import ru.feoktistov.tm.endpoint.ProjectEndpoint;
import ru.feoktistov.tm.endpoint.TaskEndpoint;
import ru.feoktistov.tm.repository.ProjectRepository;
import ru.feoktistov.tm.repository.TaskRepository;
import ru.feoktistov.tm.service.DomainService;
import ru.feoktistov.tm.service.ProjectService;
import ru.feoktistov.tm.service.PropertyService;
import ru.feoktistov.tm.service.TaskService;


import javax.xml.ws.Endpoint;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

import static ru.feoktistov.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {

    private final ConnectionProvider connectionProvider = new ConnectionProvider() {
        @Override
        public Connection getConnection() {
            try {
                final String jdbcURL = propertyService.getJdbcURL();
                final String username = propertyService.getJDBCUsername();
                final String password = propertyService.getJDBCPassword();
                return DriverManager.getConnection(jdbcURL, username, password);
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    };



    private final ProjectService projectService = new ProjectService(connectionProvider);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskService taskService = new TaskService(connectionProvider);

    private final TaskController taskController = new TaskController(taskService);

    private final DomainService domainService = new DomainService(projectService, taskService);



    private final SystemController systemController = new SystemController(domainService);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService);

    private static final PropertyService propertyService = new PropertyService();


    public static void main(final String[] args) throws Exception{



        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", app.taskEndpoint);
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", app.projectEndpoint);
        app.systemController.displayWelcome();

        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) throws Exception{
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) throws Exception{
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return  systemController.displayAbout();
            case HELP: return  systemController.displayHelp();
            case EXIT: return  systemController.displayExit();

            case DATA_EXPORT_BASE64: return systemController.exportBASE64();
            case DATA_EXPORT_JSON: return systemController.exportJSON();
            case DATA_EXPORT_XML: return systemController.exportXML();

            case DATA_IMPORT_BASE64: return systemController.importBASE64();
            case DATA_IMPORT_JSON: return systemController.importJSON();
            case DATA_IMPORT_XML: return systemController.importXML();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();

            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();


            case TASK_LIST: return taskController.listTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();

            default: return systemController.displayError();
        }
    }




}
