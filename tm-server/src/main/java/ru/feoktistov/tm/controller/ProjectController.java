package ru.feoktistov.tm.controller;

import ru.feoktistov.tm.entity.Project;
import ru.feoktistov.tm.service.ProjectService;

import java.sql.SQLException;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    public int createProject() throws SQLException {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
        return 0;
    }


    public int removeProjectByName() throws SQLException {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        projectService.removeByName(name);
         System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById() throws SQLException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        projectService.removeById(id);
         System.out.println("[OK]");
        return 0;
    }


    public int clearProject() throws SQLException {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listProject() throws SQLException {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project: projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }


    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: "+ project.getId());
        System.out.println("NAME: "+ project.getName());
        System.out.println("DESCRIPTION: "+ project.getDescription());
        System.out.println("[OK]");
    }

}
